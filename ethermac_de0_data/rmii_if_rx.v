`timescale 1ns / 1ps
//*****************************************************************************
// File Name            : rmii_if_rx.v
//-----------------------------------------------------------------------------
// Function             : rgmii->rmii
//                        
//-----------------------------------------------------------------------------
// Designer             : yokomizo 
//-----------------------------------------------------------------------------
// History
// -.-- 2011/9/23
// -.-- 2012/2/8
//*****************************************************************************
module rmii_if_rx
(       reset,
        clk_50m_pll,
        phy_rx_clk,
        rx_clk,
        rgmii_in,
        rx_control,
        rmii_rxd,
        rmii_rx_en
); 

   input   reset;
   input   clk_50m_pll;
   input   phy_rx_clk;
   input   rx_clk;
   output [3:0] rgmii_in;
   output  rx_control;
   input  [1:0]         rmii_rxd;
   input                rmii_rx_en;
   
 
  reg [1:0] rmii_rxd_cb;
  reg [1:0] rmii_rxd_i_d1;
  reg [1:0] rmii_rxd_i_d2;
  reg [1:0] rmii_rxd_i_d3;
  reg [1:0] rmii_rxd_i_d4;
  reg [1:0] rmii_rxd_i_d5;
  reg [1:0] rmii_rxd_i_d6;
  reg [1:0] rmii_rxd_i_d7;
  reg [1:0] rmii_rxd_i_d8;
  reg    rmii_rx_en_cb;
  reg    rmii_rx_en_i_d1;
  reg    rmii_rx_en_i_d2;
  reg    rmii_rx_en_i_d3;
  reg    rmii_rx_en_i_d4;
  reg    rmii_rx_en_i_d5;
  reg    rmii_rx_en_i_d6;
  reg    rmii_rx_en_i_d7;
  reg    rmii_rx_en_i_d8;
 
  reg [1:0] rmii_rxd_i;
  reg    rmii_rx_en_i;
  wire [1:0]   rmii_rxd_fo;
  reg          rmii_rx_en_fo;
  reg         rdreq;
  wire         rdempty;
  wire         wrfull;
   
  reg [1:0] rmii_rxd_d1;
  reg [1:0] rmii_rxd_d2;
  reg [1:0] rmii_rxd_d3;
  reg [1:0] rmii_rxd_d4;
  reg [1:0] rmii_rxd_d5;
  reg [1:0] rmii_rxd_d6;
  reg [1:0] rmii_rxd_d7;
  reg [1:0] rmii_rxd_d8;
  reg [1:0] rmii_rxd_d9;
  reg    rmii_rx_en_d1;
  reg    rmii_rx_en_d2;
  reg    rmii_rx_en_d3;
  reg    rmii_rx_en_d4;
  reg    rmii_rx_en_d5;
  reg    rmii_rx_en_d6;
  reg    rmii_rx_en_d7;
  reg    rmii_rx_en_d8;
  reg    rmii_rx_en_d9;
  reg [2:0] rx_cnt;
  reg [15:0] rmii_rxd_h;
  reg [7:0]  rmii_rx_en_h;
  // rx_clk
  reg [15:0] rx_da_hold;
  reg [7:0]  rx_da_en_hold;
  reg [7:0]        rx_en_d1;
  reg [7:0]        rx_en_d2;
  reg [7:0]        rx_en_d3;
  reg [7:0]        rx_en_d4;
  reg [15:0] rx_da_d1;
  reg [15:0] rx_da_d2;
  reg [15:0] rx_da_d3;
  reg [15:0] rx_da_d4;
  reg [1:0]  rgmii_cnt;
  reg [3:0] rgmii_in;
  reg  rx_control;
  reg rdempty_d1;
  reg rdempty_d2;
  reg rdempty_d3;
  reg rdempty_d4;
  reg rdempty_d5;
  reg rdempty_d6;

//-------------------------------------------------------
//phy_rx_clk
//-------------------------------------------------------
/*
always @( negedge phy_rx_clk )
  begin
     rmii_rxd_cb <= rmii_rxd;
     rmii_rx_en_cb <= rmii_rx_en;
  end
*/     
always @( posedge phy_rx_clk or posedge reset)
  begin
    if (reset==1'b1) begin
      rmii_rxd_i_d1 <= 2'b00;
      rmii_rxd_i_d2 <= 2'b00;
      rmii_rxd_i_d3 <= 2'b00;
      rmii_rxd_i_d4 <= 2'b00;
      rmii_rxd_i_d5 <= 2'b00;
      rmii_rxd_i_d6 <= 2'b00;
      rmii_rxd_i_d7 <= 2'b00;
      rmii_rxd_i_d8 <= 2'b00;
    end
    else begin
      //rmii_rxd_i_d1 <=  rmii_rxd_cb;
      rmii_rxd_i_d1 <=  rmii_rxd;
      rmii_rxd_i_d2 <=  rmii_rxd_i_d1;
      rmii_rxd_i_d3 <=  rmii_rxd_i_d2;
      rmii_rxd_i_d4 <=  rmii_rxd_i_d3;
      rmii_rxd_i_d5 <=  rmii_rxd_i_d4;
      rmii_rxd_i_d6 <=  rmii_rxd_i_d5;
      rmii_rxd_i_d7 <=  rmii_rxd_i_d6;
      rmii_rxd_i_d8 <=  rmii_rxd_i_d7;
    end
  end // always @ ( posedge phy_rx_clk or posedge reset)

   
always @( posedge phy_rx_clk or posedge reset)
  begin
    if (reset==1'b1) begin
      rmii_rx_en_i_d1 <= 1'b0;
      rmii_rx_en_i_d2 <= 1'b0;
      rmii_rx_en_i_d3 <= 1'b0;
      rmii_rx_en_i_d4 <= 1'b0;
      rmii_rx_en_i_d5 <= 1'b0;
      rmii_rx_en_i_d6 <= 1'b0;
      rmii_rx_en_i_d7 <= 1'b0;
      rmii_rx_en_i_d8 <= 1'b0;
    end
    else begin
      //rmii_rx_en_i_d1 <= rmii_rx_en_cb;
      rmii_rx_en_i_d1 <= rmii_rx_en;
      rmii_rx_en_i_d2 <= rmii_rx_en_i_d1;
      if((rmii_rx_en_i_d1==1'b1)&&(rmii_rx_en_i_d2==1'b0)&&(rmii_rx_en_i_d3==1'b1))
        rmii_rx_en_i_d3 <= 1'b1;
      else
        rmii_rx_en_i_d3 <= rmii_rx_en_i_d2;
      rmii_rx_en_i_d4 <= rmii_rx_en_i_d3;
      rmii_rx_en_i_d5 <= rmii_rx_en_i_d4;
      rmii_rx_en_i_d6 <= rmii_rx_en_i_d5;
      rmii_rx_en_i_d7 <= rmii_rx_en_i_d6;
      rmii_rx_en_i_d8 <= rmii_rx_en_i_d7;
    end
  end // always @ ( posedge phy_rx_clk or posedge reset)
   
   
always @( posedge phy_rx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rmii_rxd_i <= 2'b00;
    else
      rmii_rxd_i <=  rmii_rxd_i_d8;
  end
   
always @( posedge phy_rx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rmii_rx_en_i <= 1'b0;
    else
      if (rmii_rx_en_i <= 1'b0)
        if((rmii_rxd_i_d1==2'b01)&&
           (rmii_rxd_i_d2==2'b01)&&
           (rmii_rxd_i_d3==2'b01)&&
           (rmii_rxd_i_d4==2'b01)&&
           (rmii_rxd_i_d5==2'b01)&&
           (rmii_rxd_i_d6==2'b01)&&
           (rmii_rxd_i_d7==2'b01)&&
           (rmii_rxd_i_d8==2'b01))
           rmii_rx_en_i <= 1'b1;
        else
           rmii_rx_en_i <= 1'b0;
      else 
        if( rmii_rx_en_i_d8==1'b0)
          rmii_rx_en_i <= 1'b0;
        else
          rmii_rx_en_i <= 1'b1;
   end
   
         
//assign rdreq = 1'b1;
   
fifo_2b_64w fifo_2b_64w_t(
        .data(     rmii_rxd_i),    
        .rdclk(    clk_50m_pll),         
        .rdreq(    rdreq),         
        .wrclk(    phy_rx_clk),    
        .wrreq(    rmii_rx_en_i),          
        .q(        rmii_rxd_fo),           
        .rdempty(  rdempty),  
        .wrfull(   wrfull) 
);

   
//-------------------------------------------------------
//clk_50m_pll
//-------------------------------------------------------

always @( posedge clk_50m_pll or posedge reset)
  begin
    if (reset==1'b1)begin
      rdempty_d1 <= 1'b1;
      rdempty_d2 <= 1'b1;
      rdempty_d3 <= 1'b1;
      rdempty_d4 <= 1'b1;
      rdempty_d5 <= 1'b1;
      rdempty_d6 <= 1'b1;
    end
    else
      begin
        rdempty_d1 <= rdempty;
        rdempty_d2 <= rdempty_d1;
        rdempty_d3 <= rdempty_d2;
        rdempty_d4 <= rdempty_d3;
        rdempty_d5 <= rdempty_d4;
        rdempty_d6 <= rdempty_d5;
      end
  end
  
always @( posedge clk_50m_pll or posedge reset)
  begin
    if (reset==1'b1)
      rdreq <= 1'b0;
    else
      rdreq <=  ~rdempty_d6;
  end

   
always @( posedge clk_50m_pll or posedge reset)
  begin
    if (reset==1'b1)
      rmii_rx_en_fo <= 1'b0;          
    else
      if (rdempty==1'b1)
        rmii_rx_en_fo <= 1'b0;     
      else if ( rdreq==1'b1)            
        rmii_rx_en_fo <= 1'b1;
      else 
        rmii_rx_en_fo <= rmii_rx_en_fo;   
  end
   

always @( posedge clk_50m_pll or posedge reset)
  begin
    if (reset==1'b1)
      begin
        rmii_rxd_d1 <= 2'd0;  
        rmii_rxd_d2 <= 2'd0;  
        rmii_rxd_d3 <= 2'd0;  
        rmii_rxd_d4 <= 2'd0;
        rmii_rxd_d5 <= 2'd0;
        rmii_rxd_d6 <= 2'd0;
        rmii_rxd_d7 <= 2'd0;
        rmii_rxd_d8 <= 2'd0;
        rmii_rxd_d9 <= 2'd0;
      end
    else
      begin
        if (rmii_rx_en_fo==1'b1)
          rmii_rxd_d1 <= rmii_rxd_fo;
        else
          rmii_rxd_d1 <= 2'd0;  
        rmii_rxd_d2 <= rmii_rxd_d1;  
        rmii_rxd_d3 <= rmii_rxd_d2;  
        rmii_rxd_d4 <= rmii_rxd_d3;
        rmii_rxd_d5 <= rmii_rxd_d4;
        rmii_rxd_d6 <= rmii_rxd_d5;
        rmii_rxd_d7 <= rmii_rxd_d6;
        rmii_rxd_d8 <= rmii_rxd_d7;
        rmii_rxd_d9 <= rmii_rxd_d8;
      end
  end
 
always @( posedge clk_50m_pll or posedge reset)
  begin
    if (reset==1'b1)
      begin
        rmii_rx_en_d1 <= 1'b0;
        rmii_rx_en_d2 <= 1'b0;
        rmii_rx_en_d3 <= 1'b0;
        rmii_rx_en_d4 <= 1'b0;
        rmii_rx_en_d5 <= 1'b0;
        rmii_rx_en_d6 <= 1'b0;
        rmii_rx_en_d7 <= 1'b0;
        rmii_rx_en_d8 <= 1'b0;
        rmii_rx_en_d9 <= 1'b0;
      end
    else
      begin
        rmii_rx_en_d1 <= rmii_rx_en_fo;
        rmii_rx_en_d2 <= rmii_rx_en_d1;
        rmii_rx_en_d3 <= rmii_rx_en_d2;
        rmii_rx_en_d4 <= rmii_rx_en_d3;
        rmii_rx_en_d5 <= rmii_rx_en_d4;
        rmii_rx_en_d6 <= rmii_rx_en_d5;
        rmii_rx_en_d7 <= rmii_rx_en_d6;
        rmii_rx_en_d8 <= rmii_rx_en_d7;
        rmii_rx_en_d9 <= rmii_rx_en_d8;
      end  
  end

always @( posedge clk_50m_pll or posedge reset)
  begin
    if (reset==1'b1)
       rx_cnt <= 3'h0;  
    else
      if ((rmii_rx_en_d2==1'b0)&&(rmii_rx_en_d1==1'b1))
        rx_cnt <= 3'h0;
      else
        if (rx_cnt == 3'h7)  
          rx_cnt <= 3'h0;
       else
          rx_cnt <= rx_cnt + 2'h1;
  end

always @( posedge clk_50m_pll or posedge reset)
  begin
    if (reset==1'b1)
      begin
        rmii_rxd_h <= 16'h000;
        rmii_rx_en_h <= 1'b0;
      end 
    else
      if (rx_cnt==3'h7)
        begin
          rmii_rxd_h <= {rmii_rxd_d9,rmii_rxd_d8,rmii_rxd_d7,rmii_rxd_d6,rmii_rxd_d5,rmii_rxd_d4,rmii_rxd_d3,rmii_rxd_d2};
          rmii_rx_en_h <= {rmii_rx_en_d9,rmii_rx_en_d8,rmii_rx_en_d7,rmii_rx_en_d6,rmii_rx_en_d5,rmii_rx_en_d4,rmii_rx_en_d3,rmii_rx_en_d2};
        end
      else
        begin  
          rmii_rxd_h <= rmii_rxd_h ; 
          rmii_rx_en_h <= rmii_rx_en_h;
        end
  end

//-------------------------------------------------------
//rx_clk
//-------------------------------------------------------
/*
wire [15:0] rmii_rxd_h_d ;
wire [7:0]rmii_rx_en_h_d ;
assign #2 rmii_rxd_h_d = rmii_rxd_h;
assign #0 rmii_rx_en_h_d = rmii_rx_en_h;
*/ 

always @( posedge rx_clk or posedge reset)
  begin
    if (reset==1'b1)
      begin
        rx_da_d1 <= 16'h0000;
        rx_da_d2 <= 16'h0000;
        rx_da_d3 <= 16'h0000;
        rx_da_d4 <= 16'h0000;
        rx_en_d1 <= 8'b0;
        rx_en_d2 <= 8'b0;
        rx_en_d3 <= 8'b0;
        rx_en_d4 <= 8'b0;
      end
    else
      begin
        rx_da_d1 <= rmii_rxd_h;
        rx_da_d2 <= rx_da_d1;
        rx_da_d3 <= rx_da_d2;
        rx_da_d4 <= rx_da_d3;
        rx_en_d1 <= rmii_rx_en_h;
        rx_en_d2 <= rx_en_d1;
        rx_en_d3 <= rx_en_d2;
        rx_en_d4 <= rx_en_d3;
      end // else: !if(reset==1'b1)
  end // always @ ( posedge rx_clk or posedge reset)
   
always @( posedge rx_clk or posedge reset)
  begin
    if (reset==1'b1)
       rgmii_cnt <= 2'h0;  
    else
      if ((rx_en_d2[0]==1'b0)&&(rx_en_d1[0]==1'b1))
        rgmii_cnt <= 2'h0;
      else
        if ((rx_en_d2[0]==1'b1)||(rx_en_d4[7]==1'b1))
          if (rgmii_cnt == 2'h3)  
            rgmii_cnt <= 2'h0;
          else
            rgmii_cnt <= rgmii_cnt + 2'h1;
        else
          rgmii_cnt <= 2'h3;
  end


always @( posedge rx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rx_da_hold <= 16'h0000;
    else
      if ( rgmii_cnt == 2'h0)
         rx_da_hold <=  rmii_rxd_h;
      else
         rx_da_hold <=  rx_da_hold;
  end
   
always @( posedge rx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rx_da_en_hold <= 8'h00;
    else
      if ( rgmii_cnt == 2'd0)
        rx_da_en_hold <=   rmii_rx_en_h;
      else
        rx_da_en_hold <=  rx_da_en_hold;
  end      
   
always @( posedge rx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rgmii_in <= 4'b0;          
    else
      case(rgmii_cnt)
        /*
        2'h1: rgmii_in <= rx_da_hold[15:12]; 
        2'h2: rgmii_in <= rx_da_hold[11:8]; 
        2'h3: rgmii_in <= rx_da_hold[7:4]; 
        2'h0: rgmii_in <= rx_da_hold[3:0]; 
        */
        2'h1: rgmii_in <= {rx_da_hold[13:12],rx_da_hold[15:14]}; 
        2'h2: rgmii_in <= {rx_da_hold[9:8],rx_da_hold[11:10]}; 
        2'h3: rgmii_in <= {rx_da_hold[5:4],rx_da_hold[7:6]}; 
        2'h0: rgmii_in <= {rx_da_hold[1:0],rx_da_hold[3:2]}; 
        default: rgmii_in <= 4'h0;
      endcase
  end

always @( posedge rx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rx_control <= 4'b0;          
    else
      case(rgmii_cnt)
        2'h1: rx_control <= rx_da_en_hold [7]; 
        2'h2: rx_control <= rx_da_en_hold [5]; 
        2'h3: rx_control <= rx_da_en_hold [3]; 
        2'h0: rx_control <= rx_da_en_hold [1]; 
        default: rx_control <= 1'b0;
      endcase
  end 


 endmodule
