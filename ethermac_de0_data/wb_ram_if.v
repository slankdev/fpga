//*****************************************************************************
// File Name            : wb_ram_if.v
//-----------------------------------------------------------------------------
// Function             : MicroBlaze_MAC Iobus to block RAM 
//                        
//-----------------------------------------------------------------------------
// Designer             : yokomizo 
//-----------------------------------------------------------------------------
// History
// -.-- 2013/08/20
//*****************************************************************************
module  wb_ram_if(
//wb slave 
  wb_clk_i, wb_rst_i, wb_dat_i, wb_dat_o,   
  wb_adr_i, wb_sel_i, wb_we_i, wb_cyc_i, wb_stb_i, wb_ack_o, wb_err_o,    
//bram                
clkb,web,addrb,dinb,doutb
);
//wb slave  
input           wb_clk_i;     // WISHBONE clock
input           wb_rst_i;     // WISHBONE reset
input   [31:0]  wb_dat_i;     // WISHBONE data input
output  [31:0]  wb_dat_o;     // WISHBONE data output
output          wb_err_o;     // WISHBONE error output
input   [12:2]  wb_adr_i;     // WISHBONE address input
input    [3:0]  wb_sel_i;     // WISHBONE byte select input
input           wb_we_i;      // WISHBONE write enable input
input           wb_cyc_i;     // WISHBONE cycle input
input           wb_stb_i;     // WISHBONE strobe input
output          wb_ack_o;     // WISHBONE acknowledge output

//block ram port-B
output clkb;
output [3 : 0] web;
output [10 : 0] addrb;
output [31 : 0] dinb;
input [31 : 0]  doutb;
//
reg         wb_ack_o;     // WISHBONE acknowledge output
reg         wb_r1_ack_o;     // WISHBONE read acknowledge output
reg         wb_r2_ack_o;     // WISHBONE read acknowledge output
reg [3 : 0] web;
//reg [10 : 0] addrb;
reg [31 : 0] dinb;
   

  
//アドレス生成
/*   
always @ (posedge wb_clk_i or posedge wb_rst_i )
  if (wb_rst_i==1'b1)
    addrb <= 9'h000;
  else 
    addrb <= wb_adr_i[12:2];
*/
assign  addrb = wb_adr_i[12:2];

   
//IOバス側メモリポート書込みデータ   
always @ (posedge wb_clk_i or posedge wb_rst_i )
  if (wb_rst_i==1'b1)
    dinb <= 32'h00000000;
  else
    if ((wb_stb_i==1'b1)&&(wb_cyc_i==1'b1)&&(wb_we_i==1'b1))
      dinb <=  wb_dat_i;
    else
      dinb <= dinb;

//IOバス側メモリポート書込みイネーブル
always @ (posedge wb_clk_i or posedge wb_rst_i )
  if (wb_rst_i==1'b1)
    web <= 4'b0000;
  else 
    if ((wb_stb_i==1'b1)&&(wb_cyc_i==1'b1)&&(wb_we_i==1'b1))
      web <= wb_sel_i;
    else
      web <= 4'b0000;
     
//ack通知 
always @ (posedge wb_clk_i or posedge wb_rst_i )
  if (wb_rst_i==1'b1)
    wb_ack_o <= 1'b0;
  else
    if  ((wb_stb_i==1'b1)&&(wb_cyc_i==1'b1))
      if (wb_we_i==1'b1)
        if( wb_ack_o==1'b0)
          wb_ack_o <= 1'b1;
        else
          wb_ack_o <= 1'b0;
      else
        wb_ack_o <= wb_r1_ack_o;
    else 
      wb_ack_o <=  1'b0 ;

always @ (posedge wb_clk_i or posedge wb_rst_i )
  if (wb_rst_i==1'b1)
    wb_r1_ack_o <= 1'b0;
  else
    if  ((wb_stb_i==1'b1)&&(wb_cyc_i==1'b1)&&(wb_r1_ack_o==1'b0)&&(wb_ack_o==1'b0))
      wb_r1_ack_o <= 1'b1;
    else 
      wb_r1_ack_o <=  1'b0 ;

always @ (posedge wb_clk_i or posedge wb_rst_i )
  if (wb_rst_i==1'b1)
    wb_r2_ack_o <= 1'b0;
  else
    wb_r2_ack_o <= wb_r1_ack_o ;
    
assign  wb_err_o = 1'b0;
  
   
//読出しデータ        
assign  wb_dat_o = doutb ;

// BRAM clk
assign clkb = wb_clk_i;
   

   
endmodule