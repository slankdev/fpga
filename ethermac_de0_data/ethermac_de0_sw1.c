//*****************************************************************************
// File Name            : ethermac_de0_sw1.c
//-----------------------------------------------------------------------------
// Function             : OpenCores IP EtherIP
//
//-----------------------------------------------------------------------------
// Designer             : yokomizo
//-----------------------------------------------------------------------------
// History
// -.-- 2013/11/12
//*****************************************************************************


#ifdef __hal__
  #include "sys/alt_stdio.h"
  #include "system.h"
  // BASEADDR
  #define PWM0_BASEADDR   0x50000000
  #define PWM1_BASEADDR   0x50000010
  #define I2C_BASEADDR    0xD0000040
  #define EMAC_BASEADDR   0xD0001000
  #define BUFMEM_BASEADDR 0xD0002000
  #define PHY_ADR 0x14
  // PRINT macro
  #define M_PRINT(str)  alt_putstr(str);
  #define M_PRINTF(str, ...) printf(str, __VA_ARGS__);
#elif __microblaze__
  #include <stdio.h>
  #include "platform.h"
  #include "xbasic_types.h"
  #include "XIOModule.h"
  // BASEADDR
  #define BUFMEM_BASEADDR 0xC0002000
  #define PWM0_BASEADDR   0xD0000000
  #define PWM1_BASEADDR   0xD0000010
  #define I2C_BASEADDR    0xD0000040
  #define EMAC_BASEADDR 0xD0001000
  #define PHY_ADR 0x1e
  // PRINT macro
  #define M_PRINT(str)  print(str);
  #define M_PRINTF(str, ...) xil_printf(str, __VA_ARGS__);
#endif

#define GPO      (*(volatile unsigned int *) PIO_0_BASE)
#define PWM0_CTRL (*(volatile unsigned short int *) (PWM0_BASEADDR + 0x0))
#define PWM0_DIV  (*(volatile unsigned short int *) (PWM0_BASEADDR + 0x2))
#define PWM0_PERIOD (*(volatile unsigned short int *) (PWM0_BASEADDR + 0x4))
#define PWM0_DC (*(volatile unsigned short int *) (PWM0_BASEADDR + 0x6))

#define PWM1_CTRL (*(volatile unsigned short int *) (PWM1_BASEADDR + 0x0))
#define PWM1_DIV  (*(volatile unsigned short int *) (PWM1_BASEADDR + 0x2))
#define PWM1_PERIOD (*(volatile unsigned short int *) (PWM1_BASEADDR + 0x4))
#define PWM1_DC (*(volatile unsigned short int *) (PWM1_BASEADDR + 0x6))


#define I2C_PRERL (*(volatile unsigned char *) (I2C_BASEADDR + 0x0))
#define I2C_PRERH (*(volatile unsigned char *) (I2C_BASEADDR + 0x1))
#define I2C_CTR   (*(volatile unsigned char *) (I2C_BASEADDR + 0x2))
#define I2C_TXR   (*(volatile unsigned char *) (I2C_BASEADDR + 0x3))
#define I2C_RXR   (*(volatile unsigned char *) (I2C_BASEADDR + 0x3))
#define I2C_CR    (*(volatile unsigned char *) (I2C_BASEADDR + 0x4))
#define I2C_SR    (*(volatile unsigned char *) (I2C_BASEADDR + 0x4))


#define EMAC_MODER (*(volatile unsigned int *) (EMAC_BASEADDR + 0x0))
#define EMAC_MIIMODER  (*(volatile unsigned int *) (EMAC_BASEADDR + 0x28))
#define EMAC_MIICOMMAND  (*(volatile unsigned int *) (EMAC_BASEADDR + 0x2C))
#define EMAC_MIIADDRESS  (*(volatile unsigned int *) (EMAC_BASEADDR + 0x30))
#define EMAC_MIITX_DATA  (*(volatile unsigned int *) (EMAC_BASEADDR + 0x34))
#define EMAC_MIIRX_DATA  (*(volatile unsigned int *) (EMAC_BASEADDR + 0x38))
#define EMAC_MIISTATUS  (*(volatile unsigned int *) (EMAC_BASEADDR + 0x3c))
#define EMAC_MACADR0  (*(volatile unsigned int *) (EMAC_BASEADDR + 0x40))
#define EMAC_MACADR1  (*(volatile unsigned int *) (EMAC_BASEADDR + 0x44))

#define EMAC_TX_BD1  (*(volatile unsigned int *) (EMAC_BASEADDR + 0x400))
#define EMAC_TX_BD1_PTR  (*(volatile unsigned int *) (EMAC_BASEADDR + 0x404))
#define EMAC_TX_BD2 (*(volatile unsigned int *) (EMAC_BASEADDR + 0x408))
#define EMAC_TX_BD2_PTR  (*(volatile unsigned int *) (EMAC_BASEADDR + 0x40C))
#define EMAC_TX_BD3 (*(volatile unsigned int *) (EMAC_BASEADDR + 0x410))
#define EMAC_TX_BD3_PTR  (*(volatile unsigned int *) (EMAC_BASEADDR + 0x414))
#define EMAC_RX_BD1  (*(volatile unsigned int *) (EMAC_BASEADDR + 0x600))
#define EMAC_RX_BD1_PTR  (*(volatile unsigned int *) (EMAC_BASEADDR + 0x604))
#define EMAC_RX_BD2 (*(volatile unsigned int *) (EMAC_BASEADDR + 0x608))
#define EMAC_RX_BD2_PTR  (*(volatile unsigned int *) (EMAC_BASEADDR + 0x60C))
#define EMAC_RX_BD3 (*(volatile unsigned int *) (EMAC_BASEADDR + 0x610))
#define EMAC_RX_BD3_PTR  (*(volatile unsigned int *) (EMAC_BASEADDR + 0x614))
#define EMAC_RX_BD4 (*(volatile unsigned int *) (EMAC_BASEADDR + 0x618))
#define EMAC_RX_BD4_PTR  (*(volatile unsigned int *) (EMAC_BASEADDR + 0x61c))


int main()
{
  int lp;
  int tmp0;
  int tmp1;
  int tmp3;
  int tmp4;
  //MDIO

  M_PRINT("strat\n");

  for(lp=0;lp<10000000;lp++){
	  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x0))  = 0xffffffff;
  }

  // EMAC_MIITX_DATA = 0x6100; //loop back
  EMAC_MIITX_DATA = 0x3100; //Auto nego
  EMAC_MIIADDRESS = PHY_ADR | 0x0;
  EMAC_MIICOMMAND = 0x4; //write
  tmp0=0;
  while(tmp0!=0){
   tmp0 = EMAC_MIISTATUS & 0x2;
  }
  M_PRINT("MDIO write \n");

  EMAC_MIIADDRESS = PHY_ADR | 0x100; //mode
  EMAC_MIICOMMAND = 0x2; //read
  tmp0=0;
  while(tmp0!=0){
   tmp0 = EMAC_MIISTATUS & 0x2;
  }
  M_PRINT("MDIO read status=");
  tmp0 = EMAC_MIIRX_DATA;
  M_PRINTF("%x\n",tmp0);

  EMAC_MODER = 0x0;
  EMAC_MACADR1 = 0xACDE;
  EMAC_MACADR0 = 0x48010203;
  EMAC_TX_BD1 = 0x5800;
  EMAC_TX_BD1_PTR = 0x0;
  EMAC_TX_BD2 = 0x5800;
  EMAC_TX_BD2_PTR = 0x200;
  EMAC_TX_BD3 = 0x7800;
  EMAC_TX_BD3_PTR = 0x400;
  EMAC_RX_BD1 = 0x8000;
  EMAC_RX_BD1_PTR = 0x1000;
  EMAC_RX_BD2 = 0x8000;
  EMAC_RX_BD2_PTR = 0x1600;
  EMAC_RX_BD3 = 0x8000;
  EMAC_RX_BD3_PTR = 0x1c00;
  EMAC_RX_BD4 = 0xe000;
  EMAC_RX_BD4_PTR = 0x1e00;
  EMAC_MODER = 0xa423;
  //EMAC_MODER = 0xa463; //loop back
  // tx frame ARP
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x0))  = 0xffffffff;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x4))  = 0xffffACDE;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x8))  = 0x48010203;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0xc))  = 0x08060001;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x10)) = 0x08000604;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x14)) = 0x0001ACDE;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x18)) = 0x48010203;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x1C)) = 0xc0a80001;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x20)) = 0x00000000;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x24)) = 0x0000c0a8;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x28)) = 0x00030000;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x2c)) = 0x00000000;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x30)) = 0x00000000;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x34)) = 0x00000000;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x38)) = 0x00000000;

  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x200)) = 0xffffffff;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x204)) = 0xffffACDE;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x208)) = 0x48010203;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x20c)) = 0x08060001;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x210)) = 0x08000604;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x214)) = 0x0001ACDE;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x218)) = 0x48010203;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x21C)) = 0xc0a80001;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x220)) = 0x00000000;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x224)) = 0x0000c0a8;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x228)) = 0x00030000;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x22c)) = 0x00000000;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x230)) = 0x00000000;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x234)) = 0x00000000;
  (*(volatile unsigned int *) (BUFMEM_BASEADDR+ 0x238)) = 0x00000000;

  EMAC_TX_BD1 = 0x405800; //len
  tmp0 = EMAC_TX_BD1;
  M_PRINTF("TXBD1=%08x \n",tmp0);
  tmp1 = EMAC_RX_BD1;
  M_PRINTF("RXBD1=%08x \n",tmp1)
  EMAC_TX_BD1 = 0x40d800; //tx strat
  tmp0 = EMAC_TX_BD1;
  M_PRINTF("TX start TXBD1=%08x \n",tmp0);
  tmp1 = EMAC_RX_BD1;
  while(tmp0!=0){
   tmp0 =  EMAC_TX_BD1 & 0x8000;
  }
  tmp0 =  EMAC_TX_BD1 ;
  M_PRINTF("TX end   TXBD1=%08x \n",tmp0);
  while(tmp0!=0){
   tmp0 =  EMAC_RX_BD1 & 0x8000;
  }
  tmp0 = EMAC_RX_BD1;
  M_PRINTF("RX end   RXBD1=%08x \n",tmp0)

    M_PRINT("tx data\n");
    for(lp=0;lp<5;lp++){
    M_PRINTF("%08x ",(*(volatile unsigned int *) (BUFMEM_BASEADDR+ lp*0x10)));
    M_PRINTF("%08x ",(*(volatile unsigned int *) (BUFMEM_BASEADDR+ lp*0x10 +0x4)));
    M_PRINTF("%08x ",(*(volatile unsigned int *) (BUFMEM_BASEADDR+ lp*0x10 +0x8)));
    M_PRINTF("%08x \n",(*(volatile unsigned int *) (BUFMEM_BASEADDR+ lp*0x10 +0xC)));
    }
    M_PRINT("rx data\n");

    for(lp=0;lp<5;lp++){
    M_PRINTF("%08x ",(*(volatile unsigned int *) (BUFMEM_BASEADDR + 0x1000 + lp*0x10)));
    M_PRINTF("%08x ",(*(volatile unsigned int *) (BUFMEM_BASEADDR + 0x1000 + lp*0x10 +0x4)));
    M_PRINTF("%08x ",(*(volatile unsigned int *) (BUFMEM_BASEADDR + 0x1000 + lp*0x10 +0x8)));
    M_PRINTF("%08x \n",(*(volatile unsigned int *) (BUFMEM_BASEADDR + 0x1000 + lp*0x10 +0xc)));
    }

  while (1){

  }
  return 0;
}

