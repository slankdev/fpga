//*****************************************************************************
// File Name            : nios2e_sys.v
//-----------------------------------------------------------------------------
// Function             : camera sample application
//                        
//-----------------------------------------------------------------------------
// Designer             : yokomizo 
//-----------------------------------------------------------------------------
// History
// -.-- 2013/02/19
// -.-- 2013/06/12
//*****************************************************************************
module nios2e_sys   ( clk_50m, resetb, 
                   UART_Tx,UART_Rx,
                   led,scl,sda,               
                   //MTxClk,MTxD,MTxEn,MTxErr,MRxClk,MRxD,MRxDV,MRxErr,MColl,MCrs,
                   rmii_txd,rmii_tx_en,rmii_rxd,rmii_rx_en,
                   phy_clk_o,phy_rstb,
                   Mdc,Mdio,
                   sw0,sw1,
                   mon

                        
);
  //
  parameter p_wb_offset_low  =  32'h10000000;
  parameter p_wb_offset_hi   =  32'h2FFFFFFF;
  parameter p_wb_pwm0_low   =  p_wb_offset_low + 0;
  parameter p_wb_pwm0_hi    =  p_wb_offset_low + 15;
  parameter p_wb_pwm1_low   =  p_wb_offset_low + 16;
  parameter p_wb_pwm1_hi    =  p_wb_offset_low + 31;
  parameter p_wb_i2c_low   =   p_wb_offset_low + 64;
  parameter p_wb_i2c_hi   =    p_wb_offset_low + 127;
  parameter p_wb_emac_low =    p_wb_offset_low + 32'h0001000;
  parameter p_wb_emac_hi =     p_wb_offset_low + 32'h00017ff;
  parameter p_wb_bram_low =    p_wb_offset_low + 32'h0002000;
  parameter p_wb_bram_hi =     p_wb_offset_low + 32'h0003fff;
  //
  input clk_50m;    //clk 50MHz
  input resetb;     //reset_b
  //UART
  output UART_Tx;       //serial tx data   115200Kbps
  input  UART_Rx;       //serial rx data   115200Kbps
  //LED
  output [9:0] led;
  //I2C 
  inout          scl;
  inout          sda;
  //ether
  /*
  input           MTxClk; 
  output   [3:0]  MTxD;   
  output          MTxEn;  
  output          MTxErr; 
  input           MRxClk; 
  input    [3:0]  MRxD;   
  input           MRxDV;  
  input           MRxErr; 
  input           MColl;  
  input           MCrs;   
  */
  output   [1:0]  rmii_txd;
  output          rmii_tx_en;
  input   [1:0]   rmii_rxd;
  input           rmii_rx_en;
  output          phy_clk_o;
  output          phy_rstb; 
  inout           Mdc;   
  inout           Mdio; 
  //
  input           sw0;
  input           sw1;
  //
  output          mon;
  //
  wire  clk_50m_i;  // 50MHz 
  wire  clk_100m_i; //100MHz CLk
  wire  clk_2m_i;  // 2MHz 
  wire  clk_25m_i;  // 25MHz 
  wire  locked;
  wire  reset_loacked;  
  wire  resetb_loacked;   
  //nios2 gpio
  wire  [31:0]   gpo;
  wire  [31:0]   gpi;
  //block ram port-A
  wire      clka; 
  wire  [3:0]wea;
  wire  we_a; 
  wire  [10 : 0] addra;
  wire  [31 : 0] dina;
  wire  [31 : 0] douta;
  //block ram port-B
  wire      clkb; 
  wire  [3:0]web;
  wire  we_b; 
  wire  [10 : 0] addrb;
  wire  [31 : 0] dinb;
  wire  [31 : 0] doutb;
  //wishboen bus  nios2 -> IP
  wire         WB_CLK;         // clock 
  wire         WB_RST;         // reset (asynchronous active hi)
  wire         WB_CYC;         // cycle
  wire         WB_STB;         // strobe
  wire  [31:0] WB_ADR;         // address adr_i[1]
  wire         WB_WE;          // write enable
  wire  [31:0] WB_DAT_I;          // data output
  wire  [31:0] WB_DAT_O;          // data input
  wire  [3:0]  WB_SEL;         // byte enable
  wire         WB_ACK;         // normal bus termination
  reg          WB_ACK_DFF;         // normal bus termination
  wire         WB_ERR;         // err bus termination
  wire         WB_RTY;         // rtry
  //pwm
  /* 
  wire         cyc_i_pwm;         // cycle
  wire         stb_i_pwm;         // strobe
  wire         we_i_pwm;          // write enable
  wire  [ 7:0] dat_i_pwm;   // data output
  wire  [ 7:0] dat_o_pwm;   // data input
  wire         ack_o_pwm;         // normal bus termination
  wire         wb_pwm_sel;
  */   
  wire         cyc_i_pwm0;         // cycle
  wire         stb_i_pwm0;         // strobe
  wire         we_i_pwm0;          // write enable
  wire  [15:0] adr_i_pwm0;         // address adr_i
  wire  [15:0] dat_o_pwm0;   // data input
  wire         ack_o_pwm0;         // normal bus termination
  wire         wb_pwm0_sel;
  wire         cyc_i_pwm1;         // cycle
  wire         stb_i_pwm1;         // strobe
  wire  [15:0] adr_i_pwm1;         // address adr_i
  wire         we_i_pwm1;          // write enable
  wire  [15:0] dat_o_pwm1;   // data input
  wire         ack_o_pwm1;         // normal bus termination
  wire         wb_pwm1_sel;
              
  //emac
  wire         cyc_i_emac;         // cycle
  wire         stb_i_emac;         // strobe
  wire         we_i_emac;          // write enable
  wire  [31:0] dat_o_emac;   // data input
  wire         ack_o_emac;         // normal bus termination
  wire         err_o_emac = 0;         // normal bus termination
  wire         wb_emac_sel;
  wire         inta_emac;
  //mdio   
  wire md_pad_i;      // MII data input (from I/O cell)
  wire mdc_pad_o;     // MII Management data clock (to PHY)
  wire md_pad_o;      // MII data output (to I/O cell)
  wire md_padoe_o;    // MII data output enable (to I/O cell)
              
  //bram
  wire         cyc_i_bram;         // cycle
  wire         stb_i_bram;         // strobe
  wire         we_i_bram;          // write enable
  wire  [31:0] dat_o_bram;   // data input
  wire         ack_o_bram;         // normal bus termination
  wire         err_o_bram;         // normal bus termination
  wire         wb_bram_sel;
  reg         wb_bram_sel_mon;
  wire         inta_bram;
   
  //i2c
  wire         cyc_i_i2c;         // cycle
  wire         stb_i_i2c;         // strobe
  wire         we_i_i2c;          // write enable
  wire  [2:0] adr_i_i2c;         // address adr_i
  wire  [7:0] dat_o_i2c;   // data input
  wire         ack_o_i2c;         // normal bus termination
  wire         wb_i2c_sel;
  wire         inta_i2c;
  
  //opencores PWM signals
  wire  i_extclk;
  wire  [15:0]i_DC;
  wire  i_valid_DC;  
  wire  o_pwm0;
  wire  o_pwm1;
   
  //opencores I2C signals
  wire rstn;
  wire scl_pad_i;
  wire scl_pad_o;
  wire scl_padoe_o;
  wire sda_pad_i;
  wire sda_pad_o;
  wire sda_padoe_o;
   
  // WISHBONE etermac
  wire    [31:0]    m_wb_adr_o;
  wire     [3:0]    m_wb_sel_o;
  wire              m_wb_we_o;
  wire     [31:0]    m_wb_dat_i;
  wire    [31:0]    m_wb_dat_o;
  wire              m_wb_cyc_o;
  wire              m_wb_stb_o;
  wire               m_wb_ack_i;
  wire               m_wb_err_i;
  //
  wire     [3:0]    mtxd_pad_o;
  wire              mtxen_pad_o;
//MII
   
  wire           MTxClk; 
  wire   [3:0]  MTxD;   
  wire          MTxEn;  
  wire          MTxErr; 
  wire           MRxClk; 
  wire    [3:0]  MRxD;   
  wire           MRxDV;  
  wire    [3:0]  MRxD_i;   
  wire           MRxDV_i;  
  wire           MRxErr; 
  wire           MColl;  
  wire           MCrs;   
  //MDIO
  wire          Mdo_I; 
  wire          Mdo_O;  
  wire          Mdo_OE; 
  wire          Mdc_O;
  //
  wire   [1:0]   rmii_rxd_i;
  wire           rmii_rx_en_i;
   
  //mon
  reg mon ;
  reg scl_mon ;
  reg scl_o_mon ;
  reg scl_oe_mon ;
  reg sda_mon ;
  reg sda_oe_mon ;
  reg sda_o_mon ;
  reg sda_i_mon ;
  reg scl_fd1; 
  reg scl_o_fd1 ;
  reg scl_oe_fd1 ;
  reg sda_fd1 ;
  reg sda_oe_fd1 ;
  reg sda_o_fd1 ;
  reg sda_i_fd1 ;
  reg scl_fd2; 
  reg scl_o_fd2 ;
  reg scl_oe_fd2 ;
  reg sda_fd2 ;
  reg sda_oe_fd2 ;
  reg sda_o_fd2 ;
  reg sda_i_fd2 ;
   
  //reset 
  assign reset = ~resetb;
  assign rstn = resetb;
  assign reset_locked =((resetb==1'b0)||(locked==1'b0))?1'b1:1'b0;
  assign resetb_locked = ~reset_locked ;
  assign phy_rstb  = ~reset_locked ;  
  //gpio                  
  assign gpi = gpo;
   
  //led
  assign led = {2'b00,gpo[3:0],2'b00,o_pwm1,o_pwm0};
   
   
clk_gen clk_gen(
   .areset(reset),   
   .inclk0(clk_50m),
   .c0(clk_50m_i),   // 50MHz
   .c1(clk_100m_i),  //100MHz
   .c2(clk_2m_i),    // 2MHz
   .c3(clk_25m_i),   // 25MHz
   .locked(locked));
   
nios2e nios2e(
   .reset_reset_n(resetb_locked),
   .clk_clk(clk_100m_i),
   .uart_0_txd(UART_Tx),    
   .uart_0_rxd(UART_Rx),
   .gpo_export(gpo),
   .gpi_export(gpi),
  //wishbone nios2 -> IP
   .wb_CLK_O(WB_CLK),         // clock
   .wb_RST_O(WB_RST),         // reset 
   .wb_CYC_O(WB_CYC),         // cycle
   .wb_STB_O(WB_STB),         // strobe
   .wb_ADR_O(WB_ADR),         // address adr
   .wb_SEL_O(WB_SEL),         // byte_sel
   .wb_WE_O( WB_WE),          // write enable
   .wb_DAT_O(WB_DAT_I),       // data output
   .wb_DAT_I(WB_DAT_O),       // data input
   .wb_ACK_I(WB_ACK),         // normal bus termination
   .wb_ERR_I(WB_ERR),         // error bus termination
   .wb_RTY_I(WB_RTY)          // retry
        );

/*     
//opencores simple_gpio    
 PWM PWM_0(
  .i_wb_clk(WB_CLK),         // clock
  .i_wb_rst(WB_RST),        // reset (asynchronous active hi)
  .i_wb_cyc(cyc_i_pwm),    // cycle
  .i_wb_stb(stb_i_pwm),    // strobe
  .i_wb_adr(WB_ADR[15:0]),         // address adr_i[1]
  .i_wb_we(we_i_pwm),      // write enable
  .i_wb_data(WB_DAT_I[15:0]),     // data output
  .o_wb_data(dat_o_pwm),    // data input
  .o_wb_ack(ack_o_pwm),    // normal bus termination
  .i_extclk(i_extclk),
  .i_DC(i_DC),
  .i_valid_DC(i_valid_DC),
  .o_pwm(o_pwm)
                         );

//WihsBoneBus interconnect
   assign wb_pwm_sel = ((WB_ADR>=p_wb_pwm_low)&&(WB_ADR<=p_wb_pwm_hi))? 1'b1:1'b0;
   assign cyc_i_pwm =(wb_pwm_sel==1'b1)?WB_CYC:1'b0;
   assign stb_i_pwm =(wb_pwm_sel==1'b1)?WB_STB:1'b0;
   assign we_i_pwm  =(wb_pwm_sel==1'b1)?WB_WE:1'b0;
   assign WB_DAT_O =(wb_pwm_sel==1'b1)?{16'h000000,dat_o_pwm}:
                                          32'h00000000;
   assign WB_ACK = (wb_pwm_sel==1'b1)?ack_o_pwm:
                                      WB_STB;
   assign WB_ERR = 1'b0;
   assign WB_RTY = 1'b0;
   
// PWM inputs
   assign i_extclk =1'b0;
   assign i_DC = 16'h0000;
   assign i_valid_DC =1'b0;    
*/   
     
//opencores simple_gpio    
 PWM PWM_0(
  .i_wb_clk(WB_CLK),         // clock
  .i_wb_rst(WB_RST),        // reset (asynchronous active hi)
  .i_wb_cyc(cyc_i_pwm0),    // cycle
  .i_wb_stb(stb_i_pwm0),    // strobe
  .i_wb_adr(adr_i_pwm0),         // address adr_i[1]
  .i_wb_we(we_i_pwm0),      // write enable
  .i_wb_data(WB_DAT_I[15:0]),     // data output
  .o_wb_data(dat_o_pwm0),    // data input
  .o_wb_ack(ack_o_pwm0),    // normal bus termination
  .i_extclk(i_extclk),
  .i_DC(i_DC),
  .i_valid_DC(i_valid_DC),
  .o_pwm(o_pwm0)
                         ); 
 PWM PWM_1(
  .i_wb_clk(WB_CLK),       // clock
  .i_wb_rst(WB_RST),       // reset (asynchronous active hi)
  .i_wb_cyc(cyc_i_pwm1),    // cycle
  .i_wb_stb(stb_i_pwm1),    // strobe
  .i_wb_adr(adr_i_pwm1), // address adr_i
  .i_wb_we(we_i_pwm1),      // write enable
  .i_wb_data(WB_DAT_I[15:0]),     // data output
  .o_wb_data(dat_o_pwm1),   // data input
  .o_wb_ack(ack_o_pwm1),    // normal bus termination
  .i_extclk(i_extclk),
  .i_DC(i_DC),
  .i_valid_DC(i_valid_DC),
  .o_pwm(o_pwm1)
                         );

 i2c_master_top i2c_top (
  // wishbone interface
  .wb_clk_i(WB_CLK),
  .wb_rst_i(WB_RST),
  .arst_i(rstn),
  .wb_adr_i(adr_i_i2c),
  .wb_dat_i(WB_DAT_I[7:0]),
  .wb_dat_o(dat_o_i2c),
  .wb_we_i(we_i_i2c),
  .wb_stb_i(stb_i_i2c),
  .wb_cyc_i(cyc_i_i2c),
  .wb_ack_o(ack_o_i2c),
  .wb_inta_o(inta_i2c),

// i2c signals
  .scl_pad_i(scl_pad_i),
  .scl_pad_o(scl_pad_o),
  .scl_padoen_o(scl_padoe_o),
  .sda_pad_i(sda_pad_i),
  .sda_pad_o(sda_pad_o),
  .sda_padoen_o(sda_padoe_o)
           );
   
//WihsBoneBus interconnect
   assign wb_pwm0_sel = ((WB_ADR>=p_wb_pwm0_low)&&(WB_ADR<=p_wb_pwm0_hi))? 1'b1:1'b0;
   assign cyc_i_pwm0 =(wb_pwm0_sel==1'b1)?WB_CYC:1'b0;
   assign stb_i_pwm0 =(wb_pwm0_sel==1'b1)?WB_STB:1'b0;
   assign adr_i_pwm0 ={12'h000,WB_ADR[3:0]};
   assign we_i_pwm0  =(wb_pwm0_sel==1'b1)?WB_WE:1'b0;
   
   assign wb_pwm1_sel = ((WB_ADR>=p_wb_pwm1_low)&&(WB_ADR<=p_wb_pwm1_hi))? 1'b1:1'b0;
   assign cyc_i_pwm1 =(wb_pwm1_sel==1'b1)?WB_CYC:1'b0;
   assign stb_i_pwm1 =(wb_pwm1_sel==1'b1)?WB_STB:1'b0;
   assign adr_i_pwm1 ={12'h000,WB_ADR[3:0]};
   assign we_i_pwm1  =(wb_pwm1_sel==1'b1)?WB_WE:1'b0;

   assign wb_i2c_sel = ((WB_ADR>=p_wb_i2c_low)&&(WB_ADR<=p_wb_i2c_hi))? 1'b1:1'b0;
   assign cyc_i_i2c =(wb_i2c_sel==1'b1)?WB_CYC:1'b0;
   assign stb_i_i2c =(wb_i2c_sel==1'b1)?WB_STB:1'b0;
   assign adr_i_i2c =WB_ADR[2:0];
   assign we_i_i2c  =(wb_i2c_sel==1'b1)?WB_WE:1'b0;

                    
   assign wb_emac_sel = ((WB_ADR>=p_wb_emac_low)&&(WB_ADR<=p_wb_emac_hi))? 1'b1:1'b0;
   assign cyc_i_emac =(wb_emac_sel==1'b1)?WB_CYC:1'b0;
   assign stb_i_emac =(wb_emac_sel==1'b1)?WB_STB:1'b0;
   assign adr_i_emac =WB_ADR[2:0];
   assign we_i_emac  =(wb_emac_sel==1'b1)?WB_WE:1'b0;
  
                
   assign wb_bram_sel = ((WB_ADR>=p_wb_bram_low)&&(WB_ADR<=p_wb_bram_hi))? 1'b1:1'b0;
   assign cyc_i_bram =(wb_bram_sel==1'b1)?WB_CYC:1'b0;
   assign stb_i_bram =(wb_bram_sel==1'b1)?WB_STB:1'b0;
   assign we_i_bram  =(wb_bram_sel==1'b1)?WB_WE:1'b0;
   
                
   assign WB_DAT_O =(wb_pwm0_sel==1'b1)?{dat_o_pwm0,dat_o_pwm0}:
                    (wb_pwm1_sel==1'b1)?{dat_o_pwm1,dat_o_pwm1}:
                    (wb_i2c_sel==1'b1)?{dat_o_i2c,dat_o_i2c,dat_o_i2c,dat_o_i2c}:
                    (wb_bram_sel==1'b1)?dat_o_bram:
                    (wb_emac_sel==1'b1)?dat_o_emac:
                                        32'h00000000;

   assign WB_ACK = (WB_ACK_DFF==1'b1)?1'b1:
                   (wb_pwm0_sel==1'b1)?ack_o_pwm0:
                   (wb_pwm1_sel==1'b1)?ack_o_pwm1:
                   (wb_i2c_sel==1'b1)?ack_o_i2c:
                   (wb_bram_sel==1'b1)?ack_o_bram:
                   (wb_emac_sel==1'b1)?ack_o_emac:
                                        WB_STB;
   
always @ (posedge clk_100m_i or negedge resetb )
  begin
     if(resetb==1'b0)
         WB_ACK_DFF <= 1'b0;
     else  
       if (((wb_pwm0_sel==1'b1)&&(ack_o_pwm0==1'b1))||
           ((wb_pwm1_sel==1'b1)&&(ack_o_pwm1==1'b1))||
           ((wb_i2c_sel==1'b1)&&(ack_o_i2c==1'b1)))
         WB_ACK_DFF <= 1'b1;
       else
         if (WB_STB==1'b0)
           WB_ACK_DFF <= 1'b0;
         else
           WB_ACK_DFF <= WB_ACK_DFF ;
  end

   assign WB_ERR = 1'b0;
   assign WB_RTY = 1'b0;
    
// PWM inputs
   assign i_extclk =1'b0;
   assign i_DC = 16'h0000;
   assign i_valid_DC =1'b0;    
   
// I2C
    assign scl = scl_padoe_o ? 1'bz : scl_pad_o;
    assign sda = sda_padoe_o ? 1'bz: sda_pad_o;
    assign scl_pad_i = scl;
    assign sda_pad_i = sda;
   //mon
   
always @ (posedge clk_2m_i )
  mon <= sda_pad_i^scl_mon^scl_o_mon^scl_oe_mon^sda_mon^sda_oe_mon^sda_o_mon^WB_ADR^wb_i2c_sel ^we_i_i2c^ stb_i_i2c ^wb_bram_sel_mon;
   

always @ (posedge clk_100m_i  )
    begin
      scl_fd1    <= scl;
      scl_oe_fd1 <= scl_padoe_o;
      scl_o_fd1  <= scl_pad_o;
      sda_fd1    <= sda; 
      sda_oe_fd1 <= sda_padoe_o;
      sda_o_fd1  <= sda_pad_o;
      wb_bram_sel_mon <= wb_bram_sel; 
    end
   
always @ (posedge clk_2m_i  )
    begin
      scl_fd2    <= scl_fd1;
      scl_oe_fd2 <= scl_oe_fd1;
      scl_o_fd2  <= scl_o_fd1;
      sda_fd2    <= sda_fd1; 
      sda_oe_fd2 <= sda_oe_fd1;
      sda_o_fd2  <= sda_o_fd1;
    end
   
always @ (posedge clk_2m_i  )
    begin
      scl_mon    <= scl_fd2;
      scl_o_mon  <= scl_o_fd2;
      scl_oe_mon <= scl_oe_fd2;
      sda_mon    <= sda_fd2;
      sda_oe_mon <= sda_oe_fd2;
      sda_o_mon  <= sda_o_fd2;
    end

  
ethmac ethmac
(
  // WISHBONE common
  .wb_clk_i(WB_CLK),
  .wb_rst_i(WB_RST),
  .wb_dat_i(WB_DAT_I),
  .wb_dat_o(dat_o_emac), 

  // WISHBONE slave
 
  .wb_adr_i(WB_ADR[11:2]),
  .wb_sel_i(WB_SEL),
  .wb_we_i(we_i_emac),
  .wb_cyc_i(cyc_i_emac), 
  .wb_stb_i(stb_i_emac),
  .wb_ack_o(ack_o_emac),
  .wb_err_o(err_o_emac),
        
// WISHBONE master
  .m_wb_adr_o(m_wb_adr_o), .m_wb_sel_o(m_wb_sel_o), .m_wb_we_o(m_wb_we_o), .m_wb_dat_i(m_wb_dat_i), 
  .m_wb_dat_o(m_wb_dat_o), .m_wb_cyc_o(m_wb_cyc_o), .m_wb_stb_o(m_wb_stb_o), .m_wb_ack_i(m_wb_ack_i), 
  .m_wb_err_i(m_wb_err_i), 

  //TX
  .mtx_clk_pad_i(MTxClk), .mtxd_pad_o(MTxD), .mtxen_pad_o(MTxEn ), .mtxerr_pad_o(MTxErr),

  //RX
  .mrx_clk_pad_i(MRxClk), .mrxd_pad_i(MRxD_i), .mrxdv_pad_i(MRxDV_i), .mrxerr_pad_i(MRxErr), 
  .mcoll_pad_i(MColl),    .mcrs_pad_i(MCrs), 
  
  // MIIM
  .mdc_pad_o(Mdc_O), .md_pad_i(Mdi_I), .md_pad_o(Mdo_O), .md_padoe_o(Mdo_OE),
  
  .int_o()
);

  
   assign MTxClk = clk_25m_i;
   assign MRxClk = clk_25m_i;
   
   assign MRxD_i = (sw0==1'b0)?MRxD:MTxD;
   assign MRxDV_i = (sw0==1'b0)?MRxDV:MTxEn;

   
   assign Mdc = Mdc_O;
   assign Mdio = (Mdo_OE==1'b1)? Mdo_O:1'bz;
   assign Mdi_I = Mdio ;
   
   
wb_ram_if wb_ram_if_a
(
  // WISHBONE common
  .wb_clk_i(WB_CLK),
  .wb_rst_i(WB_RST),
  .wb_dat_i(WB_DAT_I),
  .wb_dat_o(dat_o_bram), 
  // WISHBONE slave
  .wb_adr_i(WB_ADR[12:2]),
  .wb_sel_i(WB_SEL),
  .wb_we_i(we_i_bram),
  .wb_cyc_i(cyc_i_bram), 
  .wb_stb_i(stb_i_bram),
  .wb_ack_o(ack_o_bram),
  .wb_err_o(err_o_bram), 
  //BRAM Port-B                  
  .clkb(clka), // input clkb
  .web(wea), // input [3 : 0] web
  .addrb(addra), // input [8 : 0] addrb
  .dinb(dina), // input [31 : 0] dinb
  .doutb(douta) // output [31 : 0] doutb
 
);

   
wb_ram_if wb_ram_if_b
(
  // WISHBONE common
  .wb_clk_i(WB_CLK),
  .wb_rst_i(WB_RST),
  .wb_dat_i(m_wb_dat_o),
  .wb_dat_o(m_wb_dat_i), 
  // WISHBONE slave
  .wb_adr_i(m_wb_adr_o[12:2]),
  .wb_sel_i(m_wb_sel_o),
  .wb_we_i(m_wb_we_o),
  .wb_cyc_i(m_wb_cyc_o), 
  .wb_stb_i(m_wb_stb_o),
  .wb_ack_o(m_wb_ack_i),
  .wb_err_o(m_wb_err_i), 
  //BRAM Port-B                  
  .clkb(clkb), // input clkb
  .web(web), // input [3 : 0] web
  .addrb(addrb), // input [8 : 0] addrb
  .dinb(dinb), // input [31 : 0] dinb
  .doutb(doutb) // output [31 : 0] doutb
 
);

assign wea_s = (wea==4'b0000)?1'b0:1'b1;
   
assign web_s = (web==4'b0000)?1'b0:1'b1;
   

    
ram_32b_2048w ram_32b_2048w_u0 (
  .clock_a(clka), // input clka
  .byteena_a(wea),
  .wren_a(wea_s), // input [0 : 0] wea
  .address_a(addra), // input [10 : 0] addra
  .data_a(dina), // input [31 : 0] dina
  .q_a(douta), // output [31 : 0] doutb
  .clock_b(clkb), // input clkb
  .byteena_b(web),
  .wren_b(web_s), // input [0 : 0] wea
  .address_b(addrb), // input [10 : 0] addrb
  .data_b(dinb), // input [31 : 0] dina
  .q_b(doutb) // output [31 : 0] doutb
);


rmii_if_tx rmii_if_tx(
        .reset(      mac_reset),              
        .clk_50m_pll(    clk_50m_i),           
        //.phy_tx_clk(    phy_tx_clk_d180),     
        .phy_tx_clk(     clk_50m_i),
        .tx_clk(     clk_25m_i),
        .rgmii_out(  MTxD),
        .tx_control( MTxEn), 
        .rmii_txd(   rmii_txd),   
        .rmii_tx_en( rmii_tx_en)
);

 assign phy_clk_o = clk_50m_i ;
 
   
rmii_if_rx rmii_if_rx(
        .reset(      mac_reset),  
        .clk_50m_pll(      clk_50m_i),            
        //.phy_rx_clk(    phy_rx_clk_d90),      
        .phy_rx_clk(       clk_50m_i),    
        .rx_clk(     clk_25m_i),       
        .rgmii_in(   MRxD),
        .rx_control( MRxDV),
        .rmii_rxd(   rmii_rxd_i),   
        .rmii_rx_en( rmii_rx_en_i)
);

   assign rmii_rxd_i = (sw1==1'b0)?rmii_rxd:rmii_txd;

   assign rmii_rx_en_i = (sw1==1'b0)?rmii_rx_en:rmii_tx_en;
  
endmodule




