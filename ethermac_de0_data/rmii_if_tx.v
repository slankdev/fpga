//*****************************************************************************
// File Name            : rmii_if_tx.v
//-----------------------------------------------------------------------------
// Function             : rgmii->rmii
//                        
//-----------------------------------------------------------------------------
// Designer             : yokomizo 
//-----------------------------------------------------------------------------
// History
// -.-- 2011/9/23
//*****************************************************************************
module rmii_if_tx
(       reset,
        clk_50m_pll,
        phy_tx_clk,
        tx_clk,
        rgmii_out,
        tx_control,
        rmii_txd,
        rmii_tx_en
); 

   input   reset;
   input   clk_50m_pll;
   input   phy_tx_clk;
   input   tx_clk;
   input [3:0] rgmii_out;
   input   tx_control;
   output [1:0]         rmii_txd;
   output               rmii_tx_en;
   
  //tx_clk 
  reg [3:0] rgmii_out_d1;
  reg [3:0] rgmii_out_h0;
  reg [3:0] rgmii_out_h1;
  reg [3:0] rgmii_out_h2;
  reg [3:0] rgmii_out_h3;
  reg [3:0] rgmii_out_h4;
  reg [3:0] rgmii_out_h5;
  reg rgmii_out_en_h0;
  reg rgmii_out_en_h1;
  reg rgmii_out_en_h2;
  reg rgmii_out_en_h3;
  reg rgmii_out_en_h4;
  reg rgmii_out_en_h5;
  reg       tx_control_d1;
  reg [2:0] rgmii_out_sel;
  // clk_50m_pll
  reg       tx_en_d1;
  reg       tx_en_d2;
  reg       tx_en_d3;
  reg       tx_en_d4;
  reg       tx_en_d5;
  reg       tx_en_d6;
  reg [3:0] tx_cnt;
  reg [1:0]     rmii_txd;
  reg           rmii_tx_en;
  reg [1:0]     rmii_txd_i;
  reg           rmii_tx_en_i;
  wire [1:0]     rmii_txd_fo;
  reg           rmii_tx_en_fo;
  reg [1:0]     rmii_txd_f1;
  reg           rmii_tx_en_f1;
  reg           rdreq;
  wire           rdempty;
  wire           wrfull;
  reg rdempty_d1;
  reg rdempty_d2;
  reg rdempty_d3;
  reg rdempty_d4;
  reg rdempty_d5;
  reg rdempty_d6;
  
//--------------------------------------------
//tx_clk
//--------------------------------------------
   
always @( posedge tx_clk or posedge reset)
  begin
    if (reset==1'b1)
      begin
        rgmii_out_d1 <= 4'h0;
        tx_control_d1 <= 1'b0;
      end
    else
      begin
        rgmii_out_d1 <= rgmii_out;
        tx_control_d1 <= tx_control;
      end
  end

always @( posedge tx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rgmii_out_sel <= 3'd0;
    else
      if ((tx_control==1'b1)&&(tx_control_d1==1'b0))
        rgmii_out_sel <= 3'd0;
      else
        if ( rgmii_out_sel == 3'd5)
          rgmii_out_sel <= 3'd0;
        else
          rgmii_out_sel <= rgmii_out_sel + 3'd1;
 end

always @( posedge tx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rgmii_out_h0 <= 4'h0;   
    else
      if (rgmii_out_sel ==3'd0)
        rgmii_out_h0 <= rgmii_out_d1;
      else
        rgmii_out_h0 <= rgmii_out_h0;
  end

always @( posedge tx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rgmii_out_en_h0 <= 1'b0;   
    else
      if (rgmii_out_sel ==3'd0)
        rgmii_out_en_h0 <= tx_control_d1;
      else
        rgmii_out_en_h0 <= rgmii_out_en_h0;
  end

always @( posedge tx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rgmii_out_h1 <= 4'h0;   
    else
      if (rgmii_out_sel ==3'd1)
        rgmii_out_h1 <= rgmii_out_d1;
      else
        rgmii_out_h1 <= rgmii_out_h1;
  end

always @( posedge tx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rgmii_out_en_h1 <= 1'b0;   
    else
      if (rgmii_out_sel ==3'd1)
        rgmii_out_en_h1 <= tx_control_d1;
      else
        rgmii_out_en_h1 <= rgmii_out_en_h1;
  end   

always @( posedge tx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rgmii_out_h2 <= 4'h0;   
    else
      if (rgmii_out_sel ==3'd2)
        rgmii_out_h2 <= rgmii_out_d1;
      else
        rgmii_out_h2 <= rgmii_out_h2;
  end

always @( posedge tx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rgmii_out_en_h2 <= 1'b0;   
    else
      if (rgmii_out_sel ==3'd2)
        rgmii_out_en_h2 <= tx_control_d1;
      else
        rgmii_out_en_h2 <= rgmii_out_en_h2;
  end   

always @( posedge tx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rgmii_out_h3 <= 4'h0;   
    else
      if (rgmii_out_sel ==3'd3)
        rgmii_out_h3 <= rgmii_out_d1;
      else
        rgmii_out_h3 <= rgmii_out_h3;
  end

always @( posedge tx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rgmii_out_en_h3 <= 1'b0;   
    else
      if (rgmii_out_sel ==3'd3)
        rgmii_out_en_h3 <= tx_control_d1;
      else
        rgmii_out_en_h3 <= rgmii_out_en_h3;
  end   

always @( posedge tx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rgmii_out_h4 <= 4'h0;   
    else
      if (rgmii_out_sel ==3'd4)
        rgmii_out_h4 <= rgmii_out_d1;
      else
        rgmii_out_h4 <= rgmii_out_h4;
  end

always @( posedge tx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rgmii_out_en_h4 <= 1'b0;   
    else
      if (rgmii_out_sel ==3'd4)
        rgmii_out_en_h4 <= tx_control_d1;
      else
        rgmii_out_en_h4 <= rgmii_out_en_h4;
  end   


always @( posedge tx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rgmii_out_h5 <= 4'h0;   
    else
      if (rgmii_out_sel ==3'd5)
        rgmii_out_h5 <= rgmii_out_d1;
      else
        rgmii_out_h5 <= rgmii_out_h5;
  end

always @( posedge tx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rgmii_out_en_h5 <= 1'b0;   
    else
      if (rgmii_out_sel ==3'd5)
        rgmii_out_en_h5 <= tx_control_d1;
      else
        rgmii_out_en_h5 <= rgmii_out_en_h5;
  end   

//-------------------------------------------------------
// clk_50m_pll
//-------------------------------------------------------

always @( posedge clk_50m_pll or posedge reset)
  begin
    if (reset==1'b1)
      begin
        tx_en_d1 <= 1'b0;  
        tx_en_d2 <= 1'b0;  
        tx_en_d3 <= 1'b0;  
        tx_en_d4 <= 1'b0;  
        tx_en_d5 <= 1'b0;  
        tx_en_d6 <= 1'b0; 
      end
    else
      begin
        tx_en_d1 <= tx_control_d1;  
        tx_en_d2 <= tx_en_d1;  
        tx_en_d3 <= tx_en_d2;  
        tx_en_d4 <= tx_en_d3;  
        tx_en_d5 <= tx_en_d4;  
        tx_en_d6 <= tx_en_d5;  
      end 
  end 

always @( posedge clk_50m_pll or posedge reset)
  begin
    if (reset==1'b1)
       tx_cnt <= 4'h0;  
    else
      if ((tx_en_d6==1'b0)&&(tx_en_d5==1'b1))
        tx_cnt <= 4'h0;
      else
        if (tx_cnt==4'hb)
          tx_cnt <= 4'h0;
        else  
          tx_cnt <= tx_cnt + 4'h1;
  end

always @( posedge clk_50m_pll or posedge reset)
  begin
    if (reset==1'b1)
      rmii_txd_i <= 2'b00;          
    else
      if (tx_en_d6==1'b1)
        case(tx_cnt)
          /*
          4'h0: rmii_txd_i <= rgmii_out_h0[3:2]; 
          4'h1: rmii_txd_i <= rgmii_out_h0[1:0]; 
          4'h2: rmii_txd_i <= rgmii_out_h1[3:2]; 
          4'h3: rmii_txd_i <= rgmii_out_h1[1:0];
          4'h4: rmii_txd_i <= rgmii_out_h2[3:2]; 
          4'h5: rmii_txd_i <= rgmii_out_h2[1:0];
          4'h6: rmii_txd_i <= rgmii_out_h3[3:2]; 
          4'h7: rmii_txd_i <= rgmii_out_h3[1:0];
          4'h8: rmii_txd_i <= rgmii_out_h4[3:2]; 
          4'h9: rmii_txd_i <= rgmii_out_h4[1:0];
          4'ha: rmii_txd_i <= rgmii_out_h5[3:2]; 
          4'hb: rmii_txd_i <= rgmii_out_h5[1:0];
          */
          4'h0: rmii_txd_i <= rgmii_out_h0[1:0]; 
          4'h1: rmii_txd_i <= rgmii_out_h0[3:2]; 
          4'h2: rmii_txd_i <= rgmii_out_h1[1:0]; 
          4'h3: rmii_txd_i <= rgmii_out_h1[3:2];
          4'h4: rmii_txd_i <= rgmii_out_h2[1:0]; 
          4'h5: rmii_txd_i <= rgmii_out_h2[3:2];
          4'h6: rmii_txd_i <= rgmii_out_h3[1:0]; 
          4'h7: rmii_txd_i <= rgmii_out_h3[3:2];
          4'h8: rmii_txd_i <= rgmii_out_h4[1:0]; 
          4'h9: rmii_txd_i <= rgmii_out_h4[3:2];
          4'ha: rmii_txd_i <= rgmii_out_h5[1:0]; 
          4'hb: rmii_txd_i <= rgmii_out_h5[3:2];
          endcase // case(tx_cnt)
      else
        rmii_txd_i <= 2'b00;    
  end // always @ ( posedge clk_50m_pll or posedge reset)
          
always @( posedge clk_50m_pll or posedge reset)
  begin
    if (reset==1'b1)
      rmii_tx_en_i <= 1'b0;
    else
      //rmii_tx_en <= tx_en_d6 ;
      if (tx_en_d6==1'b1)
        case(tx_cnt)
          4'h0:rmii_tx_en_i <= rgmii_out_en_h0;
          4'h1:rmii_tx_en_i <= rgmii_out_en_h0;
          4'h2:rmii_tx_en_i <= rgmii_out_en_h1;
          4'h3:rmii_tx_en_i <= rgmii_out_en_h1;
          4'h4:rmii_tx_en_i <= rgmii_out_en_h2;
          4'h5:rmii_tx_en_i <= rgmii_out_en_h2;
          4'h6:rmii_tx_en_i <= rgmii_out_en_h3;
          4'h7:rmii_tx_en_i <= rgmii_out_en_h3;
          4'h8:rmii_tx_en_i <= rgmii_out_en_h4;
          4'h9:rmii_tx_en_i <= rgmii_out_en_h4;
          4'ha:rmii_tx_en_i <= rgmii_out_en_h5;
          4'hb:rmii_tx_en_i <= rgmii_out_en_h5;    
          endcase // case(tx_cnt)
      else
        rmii_tx_en_i <= 1'b0;
  end

//--------------------------------------------
//phy_tx_clk
//--------------------------------------------
   
//assign rdreq = 1'b1;
   
fifo_2b_64w fifo_2b_64w_t(
        .data(     rmii_txd_i),    
        .rdclk(    phy_tx_clk),         
        .rdreq(    rdreq),         
        .wrclk(    clk_50m_pll),    
        .wrreq(    rmii_tx_en_i),          
        .q(        rmii_txd_fo),           
        .rdempty(  rdempty),  
        .wrfull(   wrfull) 
);

always @( posedge phy_tx_clk or posedge reset)
  begin
    if (reset==1'b1)begin
      rdempty_d1 <= 1'b1;
      rdempty_d2 <= 1'b1;
      rdempty_d3 <= 1'b1;
      rdempty_d4 <= 1'b1;
      rdempty_d5 <= 1'b1;
      rdempty_d6 <= 1'b1;
    end
    else
      begin
        rdempty_d1 <= rdempty;
        rdempty_d2 <= rdempty_d1;
        rdempty_d3 <= rdempty_d2;
        rdempty_d4 <= rdempty_d3;
        rdempty_d5 <= rdempty_d4;
        rdempty_d6 <= rdempty_d5;
      end
  end // always @ ( posedge phy_rx_clk or posedge reset)
   
always @( posedge phy_tx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rdreq <= 1'b0;          
    else
      rdreq <= ~rdempty_d6;    
  end
   
always @( posedge phy_tx_clk or posedge reset)  begin
    if (reset==1'b1)
      rmii_txd_f1 <= 2'b00;          
    else
      if (rmii_tx_en_fo==1'b1)
        rmii_txd_f1 <= rmii_txd_fo;
      else    
        rmii_txd_f1 <= 2'b00;
  end
   
always @( posedge phy_tx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rmii_tx_en_fo <= 1'b0;          
    else
      if (rdempty==1'b1)
        rmii_tx_en_fo <= 1'b0;     
      else if ( rdreq==1'b1)            
        rmii_tx_en_fo <= 1'b1;
      else 
        rmii_tx_en_fo <= rmii_tx_en_fo;   
  end
   
always @( posedge phy_tx_clk or posedge reset)
//always @( negedge phy_tx_clk or posedge reset)
  begin
    if (reset==1'b1)
      rmii_tx_en_f1 <= 1'b0;          
    else
      rmii_tx_en_f1 <= rmii_tx_en_fo;    
  end
   
always @( negedge phy_tx_clk )
  begin
      rmii_tx_en <= rmii_tx_en_f1;   
      rmii_txd <= rmii_txd_f1; 
  end
     
endmodule
