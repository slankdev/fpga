///****************************************************************************
// File Name            : avalon2wb.v
//-----------------------------------------------------------------------------
// Function             : avalon to Wishbone BUS 
//                        
//-----------------------------------------------------------------------------
// Designer             : yokomizo 
//-----------------------------------------------------------------------------
// History
// -.-- 2013/03/07
//*****************************************************************************
module avalon2wb (
csi_clockreset_clk,    // clock interface
csi_clockreset_reset_n,// reset clock interface
avs_s1_address,        // s1 slave interface
avs_s1_byteenable,     // s1 slave interface
avs_s1_read,           // s1 slave interface
avs_s1_write,          // s1 slave interface
avs_s1_writedata,      // s1 slave interface
avs_s1_readdata,       // s1 slave interface
CLK_O, RST_O, CYC_O, STB_O, ADR_O, SEL_O, WE_O, DAT_O, DAT_I, ACK_I ,ERR_I, RTY_I //wb 
                  
);
input csi_clockreset_clk;
input csi_clockreset_reset_n;
input [27:0] avs_s1_address;
input [3:0] avs_s1_byteenable;   
input avs_s1_read;
input avs_s1_write;
input [31:0] avs_s1_writedata;
output [31:0] avs_s1_readdata;
//
output         CLK_O;         // clock
output         RST_O;         // reset (asynchronous active low)
output         CYC_O;         // cycle
output         STB_O;         // strobe
output  [31:0]  ADR_O;        // address adr_o
output  [3:0]  SEL_O;         // byteenable
output         WE_O;          // write enable
output  [ 31:0] DAT_O;        // data output
input [31:0] DAT_I;           // data input
input        ACK_I;           // normal bus termination
input        ERR_I;           // errorl bus termination
input        RTY_I;           // retry
//
wire [1:0] byte_adr_b01;
reg [31:0] avs_s1_readdata;
//
reg         CYC_O;         // cycle
reg         STB_O;         // strobe
reg  [31:0]  ADR_O;         // address adr_o
reg  [3:0]  SEL_O;         // address adr_o
reg         WE_O;          // write enable
reg  [ 31:0] DAT_O;         // data output
//
wire      wb_term; //    bus termination
//   
assign CLK_O = csi_clockreset_clk;
assign RST_O = ~csi_clockreset_reset_n;
//WishBone Bus termination
assign wb_term =((ACK_I==1'b1)||(ERR_I==1'b1)||(RTY_I==1'b1))?1'b1:1'b0;
    
assign byte_adr_b01 = (avs_s1_byteenable[0]==1'b1)?2'b00:                 
                      (avs_s1_byteenable[1]==1'b1)?2'b01:
                      (avs_s1_byteenable[2]==1'b1)?2'b10:
                      (avs_s1_byteenable[3]==1'b1)?2'b11:2'b00;

        
always @ (posedge csi_clockreset_clk or negedge csi_clockreset_reset_n )
  if (csi_clockreset_reset_n==1'b0)
    CYC_O <= 1'b0;
  else 
    if ((avs_s1_read==1'b1)||(avs_s1_write==1'b1))
       CYC_O <=  1'b1 ;
    else if (wb_term==1'b1)
       CYC_O <=  1'b0 ;
    else
       CYC_O <=  CYC_O ;
    
always @ (posedge csi_clockreset_clk or negedge csi_clockreset_reset_n )
  if (csi_clockreset_reset_n==1'b0)
    STB_O <= 1'b0;
  else 
    if ((avs_s1_read==1'b1)||(avs_s1_write==1'b1))
       STB_O <=  1'b1 ;
    else if (wb_term==1'b1)
       STB_O <=  1'b0 ;
    else
       STB_O <= STB_O ;
   
always @ (posedge csi_clockreset_clk or negedge csi_clockreset_reset_n )
  if (csi_clockreset_reset_n==1'b0)
    WE_O <= 1'b0;
  else 
    if (avs_s1_write==1'b1)
       WE_O <=  1'b1 ;
    else if (wb_term==1'b1)
       WE_O <=  1'b0 ;
    else
       WE_O <= WE_O ;

always @ (posedge csi_clockreset_clk or negedge csi_clockreset_reset_n )
  if (csi_clockreset_reset_n==1'b0)
    ADR_O <= 32'h00000000;
  else 
    if ((avs_s1_read==1'b1)||(avs_s1_write==1'b1))
       ADR_O <= {2'b00,avs_s1_address[27:0],byte_adr_b01};
    else
       ADR_O <= ADR_O ;
   
always @ (posedge csi_clockreset_clk or negedge csi_clockreset_reset_n )
  if (csi_clockreset_reset_n==1'b0)
    SEL_O <= 4'b0000;
  else 
    if ((avs_s1_read==1'b1)||(avs_s1_write==1'b1))
       SEL_O <= avs_s1_byteenable;
    else
       SEL_O <= SEL_O ;

always @ (posedge csi_clockreset_clk or negedge csi_clockreset_reset_n )
  if (csi_clockreset_reset_n==1'b0)
    DAT_O <= 32'h00000000;
  else 
    if (avs_s1_write==1'b1)
       /*
       if(avs_s1_byteenable[0]==1'b1)
         DAT_O <= avs_s1_writedata;
       else if (avs_s1_byteenable[1]==1'b1)  
         DAT_O <= {8'h00,avs_s1_writedata[31:8]};
       else if (avs_s1_byteenable[2]==1'b1)  
         DAT_O <= {16'h0000,avs_s1_writedata[31:16]};
       else if (avs_s1_byteenable[3]==1'b1)  
         DAT_O <= {24'h0000,avs_s1_writedata[31:24]};
       else
       */
         DAT_O <= avs_s1_writedata;
    else
       DAT_O <= DAT_O ;
        
always @ (posedge csi_clockreset_clk or negedge csi_clockreset_reset_n )
  if (csi_clockreset_reset_n==1'b0)
    avs_s1_readdata <= 32'h00000000;
  else 
    if (ACK_I==1'b1)
      /*
      if(avs_s1_byteenable[0]==1'b1)
        avs_s1_readdata <= DAT_I;
      else if (avs_s1_byteenable[1]==1'b1)  
        avs_s1_readdata <= {DAT_I[23:0],8'h00};
      else if (avs_s1_byteenable[2]==1'b1)  
        avs_s1_readdata <= {DAT_I[15:0],16'h00};
      else if (avs_s1_byteenable[3]==1'b1)  
        avs_s1_readdata <= {DAT_I[7:0],24'h00};
      else
      */
      avs_s1_readdata <= DAT_I;
    else
      avs_s1_readdata <= avs_s1_readdata ;

        
endmodule